var request = require("request");
var oracledb = require('oracledb');
let chai = require('chai');
let should = chai.should();
var urlStart = "https://qc.sttx40.com";
var options3 = {
    url: "https://qc.sttx40.com/Auth/api/v4/login",
    headers:
        { 'postman-token': '79ad1a33-c0e6-cc59-1918-b5bf0a1b0d11',
            'cache-control': 'no-cache',
            'content-type': 'application/json' },
    body: { username: 'qc.automation',
        password: '3Ppd0ae3uHir' ,
        location: 'CTS',
        sublocation: ''},
    json: true
};


describe("Login", function()  {
    it("Should login and get a token to be used later.", function() {
        this.timeout(4000);
        return new Promise(function(resolve) {
            return request.post(options3, function (error, response, body) {

                if (error) throw new Error(error);

                options.headers = {
                    'Authorization': "Bearer " + body.data,
                };

                return resolve();
            });
        });
    });
});


var options = {
    url: urlStart + "/Specimens/api/v4/Specimens/658582",
};

describe("Test Specimens array", function()  {

    it("Should get the Specimens array", function() {
        this.timeout(4000);
        return new Promise( function(resolve) {
            return request.get(options, function (error, response, body) {

                if (error) throw new Error(error);

                let resp = JSON.parse(body);

                resp.data["0"].specimenId.should.be.eql("658582");
                resp.data["0"].patientId.should.be.eql("65858");

                return resolve();
            });
        });
    });
});