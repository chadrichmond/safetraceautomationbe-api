
 var chakram = require('chakram'),
     expect = chakram.expect;
 var urlStart = "https://qc.sttx40.com";
 //import {TestRailWidget} from "../utils/class_testRailWidget";
 var options = {
     headers:
         {
             'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJGaXJzdE5hbWUiOiJxYyIsIkxhc3ROYW1lIjoiYXV0b21hdGlvbiIsIlVzZXJJZCI6InFjLmF1dG9tYXRpb24iLCJyb2xlIjpbIkxPR0lOIiwiRURJVF9TUEVDSU1FTiJdLCJuYmYiOjE1MDE1MzEwNzksImV4cCI6MTUwMTU1MjY3OSwiaWF0IjoxNTAxNTMxMDc5LCJpc3MiOiJTYWZlVHJhY2VUeCJ9.hhx2f3qMluJYy2eHVwTkJP5OmAv7NEXJKY9lDjm2ZB',
             'cache-control': 'no-cache',
             'content-type': 'application/json' },
     json: true
 };
 var creds = {
     username: 'qc.automation',
     password: '3Ppd0ae3uHir',
     location: 'CTS',
     sublocation: ''
 };
 var badcreds = {
     username: 'bad',
     password: 'badcreds',
     location: 'CTS',
     sublocation: ''
 };
 var limitedcreds = {
     username: 'TxLimitedUser',
     password: 'P@ssw0rd',
     location: 'CTS',
     sublocation: ''
 };


 describe("Chakram", function() {
     /** https://haemoslalom.testrail.net//index.php?/cases/view/3800 **/
     it("Should reject login for bad password - Case 3800", function() {
         return chakram.post(urlStart + "/Auth/api/v4/login", badcreds).then(function(response) {
             expect(response).to.have.status(401);
         });
     });

     /** https://haemoslalom.testrail.net//index.php?/cases/view/13259 **/
     it("Should reject login for limited user - Case 13259", function() {
         return chakram.post(urlStart + "/Auth/api/v4/login", limitedcreds).then(function(response) {
             expect(response).to.have.status(403);
         });
     });

     /** https://haemoslalom.testrail.net//index.php?/cases/view/3799 **/
     it("Should login and get an auth token - Case 3799", function() {
         this.timeout(3000);
         return chakram.post(urlStart + "/Auth/api/v4/login", creds).then(function(response) {
             this.headers = {
                 'Authorization': 'Bearer ' + response.response.body.data};
             expect(response).to.have.status(200);
         });
     });

     /** https://haemoslalom.testrail.net//index.php?/cases/view/3802 **/
     it("Should logout user", function() {
         return chakram.post(urlStart + "/Auth/api/v4/logout","", options).then(function(response) {
             expect(response).to.have.status(401);
         });
     });
 });

 //TODO: Call SQL to verify
 // call SQL with params

// var connection = new ActiveXObject("ADODB.Connection") ;

// var connectionString="Data Source=//oracle-qc-1.cohykvpzbvhi.us-east-1.rds.amazonaws.com:1521/ORCL;User Id=EBIS; Password=DEBIS";

// connection.Open(connectionstring);
// var rs = new ActiveXObject("ADODB.Recordset");

// rs.Open("SELECT * FROM test", connection);
// rs.movenext
// while(!rs.eof)
 //{
 //    document.write(rs.fields(1));
//     rs.movenext;
// }

// rs.close;
// connection.close;

 // report results to TesTrail
 // Run from command line npm test all API, subsets, default and --testrail