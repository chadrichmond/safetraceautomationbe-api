"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var LoginPage = (function () {
    function LoginPage() {
        // console.log("  In constructor for 'LoginPage'");
    }
    LoginPage.prototype.initialize = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                // console.log("   In 'initialize' for 'LoginPage'");
                if (!this.initializePromise) {
                    // await GeneralUtilities.initializationMessage(null, 'LoginPage');
                    return [2 /*return*/, this.initializePromise = new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
                            var _a, _b, _c, _d, _e, _f, _g, _h;
                            return __generator(this, function (_j) {
                                switch (_j.label) {
                                    case 0:
                                        _a = this;
                                        return [4 /*yield*/, protractor_1.$('div.login img')];
                                    case 1:
                                        _a.title = _j.sent();
                                        _b = this;
                                        return [4 /*yield*/, protractor_1.$('div.login-header')];
                                    case 2:
                                        _b.loginTitle = _j.sent();
                                        _c = this;
                                        return [4 /*yield*/, protractor_1.$('div.login-icon img')];
                                    case 3:
                                        _c.loginIcon = _j.sent();
                                        _d = this;
                                        return [4 /*yield*/, protractor_1.$('label.login-username')];
                                    case 4:
                                        _d.usernameTitle = _j.sent();
                                        _e = this;
                                        return [4 /*yield*/, protractor_1.$("#username")];
                                    case 5:
                                        _e.usernameInputBox = _j.sent();
                                        _f = this;
                                        return [4 /*yield*/, protractor_1.$('label.login-password')];
                                    case 6:
                                        _f.passwordTitle = _j.sent();
                                        _g = this;
                                        return [4 /*yield*/, protractor_1.$("#password")];
                                    case 7:
                                        _g.passwordInputBox = _j.sent();
                                        _h = this;
                                        return [4 /*yield*/, protractor_1.$('div.login-footer button')];
                                    case 8:
                                        _h.loginButton = _j.sent();
                                        return [2 /*return*/, resolve()];
                                }
                            });
                        }); })];
                }
                return [2 /*return*/, this.initializePromise];
            });
        });
    };
    // This will log into the site
    LoginPage.prototype.login = function (username, password) {
        if (username === void 0) { username = ""; }
        if (password === void 0) { password = ""; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!((username === "") || (password === ""))) return [3 /*break*/, 2];
                        return [4 /*yield*/, protractor_1.browser.getCurrentUrl().then(function (url) {
                                // console.log("  URL = " + url);
                                if (url.includes("dev.sttx40")) {
                                    username = "dev.automation";
                                    password = "dI,h/p_2vx3N";
                                }
                                else if (url.includes("qc.sttx40")) {
                                    username = "qc.automation";
                                    password = "3Ppd0ae3uHir";
                                }
                                else {
                                    throw "Sorry - don't have login info for '" + url + "'";
                                }
                            })];
                    case 1:
                        _a.sent();
                        _a.label = 2;
                    case 2: 
                    // console.log(`   In 'login(${username}, ${password})'`);
                    return [4 /*yield*/, this.usernameInputBox.sendKeys(username)];
                    case 3:
                        // console.log(`   In 'login(${username}, ${password})'`);
                        _a.sent();
                        return [4 /*yield*/, this.passwordInputBox.sendKeys(password)];
                    case 4:
                        _a.sent();
                        return [2 /*return*/, this.loginButton.click().then(function () {
                                return protractor_1.$('div.error-message').isPresent().then(function (errorMessagePresent) {
                                    if (errorMessagePresent) {
                                        throw "Credentials for login didn't work - CANNOT PROCEED!";
                                    }
                                    else {
                                        // return console.log("Done logging in...");
                                    }
                                });
                            })];
                }
            });
        });
    };
    return LoginPage;
}());
exports.LoginPage = LoginPage;
