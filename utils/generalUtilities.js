"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class GeneralUtilities {
    // This will output the CSS class of the element passed in, along with the class type that's being initialized
    static initializationMessage(element, classBeingInitialized = "NEEDS INFO") {
        return new Promise((resolve) => {
            if (element === null) {
                return resolve(console.log(`     ... Initializing basic details of '${classBeingInitialized}'`));
            }
            else {
                return element.getAttribute('class').then((elementClass) => {
                    return resolve(console.log(`     ... Initializing basic details of '${classBeingInitialized}' for element "${elementClass}"`));
                });
            }
        });
    }
}
exports.GeneralUtilities = GeneralUtilities;
//# sourceMappingURL=generalUtilities.js.map