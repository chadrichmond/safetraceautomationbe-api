"use strict";
//// Ref: https://github.com/angular/protractor/blob/master/lib/config.ts
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
const login_1 = require("../objects/login");
let timeoutMS = 59000;
exports.config = {
    framework: 'jasmine',
    capabilities: {
        browserName: 'chrome'
        //browserName: 'internet explorer'
    },
    restartBrowserBetweenTests: false,
    seleniumAddress: 'http://127.0.0.1:4444/wd/hub',
    allScriptsTimeout: timeoutMS,
    jasmineNodeOpts: {
        defaultTimeoutInterval: timeoutMS
    },
    useAllAngular2AppRoots: true,
    // baseUrl: "https://dev.sttx40.com/",     // for testing on the dev environment
    baseUrl: "https://qc.sttx40.com/",
    /****
     *   Add tests in the 'specs' section
     ****/
    specs: [
        '../specs/**/*.js'
    ],
    onPrepare: () => {
        // console.log("  PREPARING TESTS");
        protractor_1.browser.manage().window().maximize();
        protractor_1.browser.manage().timeouts().implicitlyWait(5000);
        protractor_1.browser.ignoreSynchronization = true;
        // Logs into the site
        return protractor_1.browser.get('/').then((thisPromise) => {
            let loginPage = new login_1.LoginPage();
            return loginPage.initialize().then(() => {
                return loginPage.login();
            }).then(() => {
                return thisPromise;
            });
        });
    },
    onComplete: () => {
        return protractor_1.browser.getProcessedConfig().then((config) => {
            // console.log('  Finished tests for this capability: ' + config.capabilities);
        });
    },
    resultJsonOutputFile: 'report.json',
    onCleanUp: (exitCode) => {
        if (exitCode === 0) {
            // console.log("ALL TESTS PASSED")
        }
        else {
            // console.log("HAD A FAILURE!!  'exitCode' = " + exitCode);
        }
    },
    afterLaunch: () => {
        /** TODO: get the code below running w/ input from the command line.
         *        We don't want it running unless specifically asked for
         *        In the meantime, it will have to be uncommented when you want it to update TestRail
         **/
        // // The code in this Promise is meant to update TestRails
        // return new Promise<void>(async resolve => {
        //     console.log("\n\n\nAbout to update TestRails with data from this run");
        //
        //     let testRail = new TestRailWidget;
        //     await testRail.update();
        //
        //     console.log("\nTestRails has been updated");
        //     console.log("\n\n\n\n");
        //     return resolve();
        // });
    },
    noGlobals: true
};
//# sourceMappingURL=defaultConfig.js.map