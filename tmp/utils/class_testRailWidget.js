"use strict";
//  This will integrate the automated test cases into the Test Rail test cases
//  Haemonetics TestRails site -> https://haemoslalom.testrail.net
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
class TestRailWidget {
    constructor() {
        // console.log("  In constructor for 'TestRailWidget'");
        this.host = 'https://haemoslalom.testrail.net';
        this.username = 'slalom.automated.tester+1@gmail.com';
        this.password = 'hjaZ54xdypVJzXgS';
        this.projectID = 1;
        this.TestRail = require('testrail-promise');
    }
    /** This method does all the heavy lifting...
     *      - gets the automated test cases from TestRail
     *      - parses the automated test results JSON file
     *      - compares the two arrays and updates the TestRail test cases as is appropriate
     */
    update() {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("   In 'update()' for 'TestRailWidget'");
            return new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
                this.trInterface = yield new this.TestRail(this.host, this.username, this.password);
                let testCases = yield this.getTestCasesThatShouldBeAutomated();
                let jsonResults = yield this.getParsedJsonFile();
                let elapsedTime = "1s";
                // We want to look for results that took some time to run (0 means it was skipped) that has 'Case' in it
                for (let result of jsonResults) {
                    if (result.duration > 0 && result.description.includes(" - Case ")) {
                        let testCaseNumber = yield result.description.match(/\d+$/);
                        for (let testCase of testCases) {
                            if (testCase.case_id == testCaseNumber) {
                                console.log(`\tFound matching test cases - ${testCaseNumber} ... updating TC ${testCase.id}`);
                                let comments = "";
                                let assignee = null;
                                let status = this.getStatusId(result.assertions[0].passed);
                                // console.log(`status = ${status}`);
                                if (status === 5) {
                                    console.log("FAILURE!!!");
                                    assignee = 1; // Hal Deranek
                                    comments = this.getErrorMessages(result);
                                    // console.log(`comments = ...\n${comments}`);
                                }
                                else {
                                    comments = "Tested on QC via test automation (not a human)";
                                }
                                let resultData = {
                                    "test_id": testCase.id,
                                    "status_id": status,
                                    "comment": comments,
                                    "assignedto_id": assignee,
                                    "elapsed": elapsedTime
                                };
                                yield this.trInterface.addResult(resultData);
                            }
                        }
                    }
                }
                return resolve();
            }));
        });
    }
    /** Tests can have multiple error messages.  This will return them all as one string
     */
    getErrorMessages(element) {
        // console.log("   In 'getErrorMessages()' for 'TestRailWidget'");
        let masterMessage;
        for (let message of element.assertions) {
            masterMessage += message.errorMsg + "\n\n";
        }
        return masterMessage;
    }
    /** Returns the first Milestone ID number that:
     *      - has been started (value 'is_started' === true)
     *      - is not completed (value 'is_completed' === false)
     *  If these conditions are not met, an error will be thrown
     */
    getCurrentMilestoneId() {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("   In 'getCurrentMilestoneId()' for 'TestRailWidget'");
            return this.trInterface.getMilestones({ "project_id": this.projectID }).then((milestones) => __awaiter(this, void 0, void 0, function* () {
                for (let i = 0; i <= milestones.length - 1; i++) {
                    let milestone = yield milestones[i];
                    if (milestone.is_completed === false && milestone.is_started === true) {
                        // console.log(`\tFound a current Milestone: ${milestone.id}`);
                        return milestone.id;
                    }
                }
                throw "Didn't find a milestone that (A) has been started and (B) hasn't been completed";
            }));
        });
    }
    /** Returns the first Test Plan ID number that:
     *      - matches the current Milestone ID (which is passed in)
     *      - is not completed (value 'is_completed' === false)
     *  If these conditions are not met, an error will be thrown
     */
    getCurrentTestPlanId(milestoneID) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log(`   In 'getCurrentTestPlanId(milestoneID)' for 'TestRailWidget'   ...  milestoneID = ${milestoneID}`);
            return this.trInterface.getPlans({ "project_id": this.projectID }).then((plans) => __awaiter(this, void 0, void 0, function* () {
                for (let i = 0; i <= plans.length - 1; i++) {
                    let plan = yield plans[i];
                    if (plan.milestone_id === milestoneID && plan.is_completed === false) {
                        // console.log(`\tFound a current Plan: ${plan.id}`);
                        return plan.id;
                    }
                }
                throw "Didn't find a plan that (A) has a milestone ID that matches the current milestone ID and (B) hasn't been completed";
            }));
        });
    }
    /** Returns the first Run ID number that has the name 'FE - Regression Tests'
     *  If this condition is not met, an error will be thrown
     */
    getCurrentRunId() {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("   In 'getCurrentRunId()' for 'TestRailWidget'");
            return this.getCurrentMilestoneId().then(milestoneID => {
                return this.getCurrentTestPlanId(milestoneID).then(testPlanID => {
                    return this.trInterface.getPlan({ "plan_id": testPlanID }).then((plan) => __awaiter(this, void 0, void 0, function* () {
                        let entries = yield plan.entries;
                        for (let i = 0; i <= entries.length - 1; i++) {
                            let entry = yield entries[i];
                            if (entry.name === 'FE - Regression Tests') {
                                // console.log(`\tFound a current Run: ${entry.runs[0].id}`);
                                return entry.runs[0].id;
                            }
                        }
                        throw "Didn't find a run with the name 'FE - Regression Tests'";
                    }));
                });
            });
        });
    }
    /** Returns the test cases based on the Current Run ID (which should be based off a run named like 'FE - regression')
     *  If no test cases are found, an error will be thrown
     */
    getCurrentRegressionTestCases() {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("   In 'getCurrentRegressionTestCases()' for 'TestRailWidget'");
            return this.getCurrentRunId().then((runID) => __awaiter(this, void 0, void 0, function* () {
                yield runID;
                return this.trInterface.getTests({ "run_id": runID }).then((testCases) => __awaiter(this, void 0, void 0, function* () {
                    yield testCases;
                    if (testCases.length === 0) {
                        throw "Didn't find any test cases in the current FE regression run";
                    }
                    else {
                        console.log(`\t${testCases.length} total test cases found in the current FE regression run`);
                    }
                    return testCases;
                }));
            }));
        });
    }
    /** Returns the test cases that are marked as 'automated' in Test Rail
     *  If no test cases are found, an error will be thrown
     */
    getTestCasesThatShouldBeAutomated() {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("   In 'getTestCasesThatShouldBeAutomated()' for 'TestRailWidget'");
            let parsedCases = [];
            return this.getCurrentRegressionTestCases().then((testCases) => __awaiter(this, void 0, void 0, function* () {
                for (let i = 0; i <= testCases.length - 1; i++) {
                    let testCase = yield testCases[i];
                    // if (testCase.custom_automated === true && testCase.status_id === 3) { // Untested
                    if (testCase.custom_automated) {
                        // console.log(`\tFound a test case that should be automated ... \n${testCase}`);
                        parsedCases.push(testCase);
                    }
                }
                console.log(`\t${parsedCases.length} total test cases found that should be automated`);
                return parsedCases;
            }));
        });
    }
    /** Finds the 'report.json' results file that is generated by the test run
     *  Returns a parsed JSON file for use
     */
    getParsedJsonFile(callback) {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("   In 'getParsedJsonFile()' for 'TestRailWidget'");
            return JSON.parse(fs.readFile('report.json'));
        });
    }
    /** Takes in a result from the JSON file.
     *   - If the result is TRUE, then it passed and 1 is returned
     *   - If the result is FALSE, then it failed and 5 is returned
     *   - If somehow it's neither, a 3 is returned (untested)
     */
    getStatusId(result) {
        // console.log("   In 'getStatusId(result)' for 'TestRailWidget' ... result = " + result);
        if (result === true) {
            return 1;
        }
        else if (result === false) {
            return 5;
        }
        else {
            return 3;
        }
    }
}
exports.TestRailWidget = TestRailWidget;
//# sourceMappingURL=class_testRailWidget.js.map