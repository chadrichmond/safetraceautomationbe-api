"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const protractor_1 = require("protractor");
class LoginPage {
    constructor() {
        // console.log("  In constructor for 'LoginPage'");
    }
    initialize() {
        return __awaiter(this, void 0, void 0, function* () {
            // console.log("   In 'initialize' for 'LoginPage'");
            if (!this.initializePromise) {
                // await GeneralUtilities.initializationMessage(null, 'LoginPage');
                return this.initializePromise = new Promise((resolve) => __awaiter(this, void 0, void 0, function* () {
                    this.title = yield protractor_1.$('div.login img');
                    this.loginTitle = yield protractor_1.$('div.login-header');
                    this.loginIcon = yield protractor_1.$('div.login-icon img');
                    this.usernameTitle = yield protractor_1.$('label.login-username');
                    this.usernameInputBox = yield protractor_1.$("#username");
                    this.passwordTitle = yield protractor_1.$('label.login-password');
                    this.passwordInputBox = yield protractor_1.$("#password");
                    this.loginButton = yield protractor_1.$('div.login-footer button');
                    return resolve();
                }));
            }
            return this.initializePromise;
        });
    }
    // This will log into the site
    login(username = "", password = "") {
        return __awaiter(this, void 0, void 0, function* () {
            if ((username === "") || (password === "")) {
                yield protractor_1.browser.getCurrentUrl().then((url) => {
                    // console.log("  URL = " + url);
                    if (url.includes("dev.sttx40")) {
                        username = "dev.automation";
                        password = "dI,h/p_2vx3N";
                    }
                    else if (url.includes("qc.sttx40")) {
                        username = "qc.automation";
                        password = "3Ppd0ae3uHir";
                    }
                    else {
                        throw `Sorry - don't have login info for '${url}'`;
                    }
                });
            }
            // console.log(`   In 'login(${username}, ${password})'`);
            yield this.usernameInputBox.sendKeys(username);
            yield this.passwordInputBox.sendKeys(password);
            return this.loginButton.click().then(() => {
                return protractor_1.$('div.error-message').isPresent().then((errorMessagePresent) => {
                    if (errorMessagePresent) {
                        throw "Credentials for login didn't work - CANNOT PROCEED!";
                    }
                    else {
                        // return console.log("Done logging in...");
                    }
                });
            });
        });
    }
}
exports.LoginPage = LoginPage;
//# sourceMappingURL=login.js.map