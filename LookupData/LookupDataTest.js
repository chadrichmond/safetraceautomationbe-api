var request = require("request");
var oracledb = require('oracledb');
let chai = require('chai');
let should = chai.should();
var urlStart = "https://qc.sttx40.com";
var options3 = {
    url: "https://qc.sttx40.com/Auth/api/v4/login",
    headers:
        { 'postman-token': '79ad1a33-c0e6-cc59-1918-b5bf0a1b0d11',
            'cache-control': 'no-cache',
            'content-type': 'application/json' },
    body: { username: 'qc.automation',
        password: '3Ppd0ae3uHir' ,
        location: 'CTS',
        sublocation: ''},
    json: true
};


describe("Login", function()  {
    it("Should login and get a token to be used later.", function() {
        return new Promise(function(resolve) {
            return request.post(options3, function (error, response, body) {

                if (error) throw new Error(error);

                options.headers = {
                    'Authorization': "Bearer " + body.data,
                };

                return resolve();
            });
        });
    });
});


var options = {
    url: urlStart + '/LookupData/api/v4/languages/en_us/lookupvalues',
};




describe("Test ABOS array", function()  {

    it("Should get the ABOS array", function() {
        this.timeout(4000);
        return new Promise( function(resolve) {
            return request.get(options, function (error, response, body) {

                if (error) throw new Error(error);

                let resp = JSON.parse(body);

                var abo = ["A","B","O","AB","hB","Oh","hA","ab","U"];

                for (i = 0; i < resp.data["0"].abos.length; i++) {
                    resp.data["0"].abos[i].aboCode.should.be.eql(abo[i]);
                }
                resp.data["0"].abos.length.should.be.eql(9);
                resp.data.should.be.a('array');
                resp.data.length.should.be.eql(1);
                return resolve();
            });
        });
    });
});

describe("Test Language array", function()  {

    it("Should get the languages array", function() {
        return new Promise( function(resolve) {
            return request.get(urlStart + '/LookupData/api/v4/languages', options, function (error, response, body) {

                if (error) throw new Error(error);

                let resp = JSON.parse(body);

                resp.data["0"]["0"].languageId.should.be.eql(0);
                resp.data["0"]["0"].locale.should.be.eql("en_us");
                resp.data["0"]["1"].languageId.should.be.eql(1);
                resp.data["0"]["1"].locale.should.be.eql("en_ca");

                return resolve();
            });
        });
    });
});


describe("Test globalsettings ", function()  {

    it("Should get the global settings", function() {
        this.timeout(4000);
        return new Promise( function(resolve) {
            return request.get(urlStart + '/LookupData/api/v4/globalsettings', options, function (error, response, body) {

                if (error) throw new Error(error);

                let resp = JSON.parse(body);

                resp.data["0"].globalSettingsDataContract.maxInactivityMinutes.should.be.eql(60);
                resp.data["0"].globalSettingsDataContract.locale.should.be.eql("en_us");

                return resolve();
            });
        });
    });
});