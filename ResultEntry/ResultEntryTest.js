var request = require("request");
var oracledb = require('oracledb');
let chai = require('chai');
let should = chai.should();
var urlStart = "https://qc.sttx40.com";
var options3 = {
    url: "https://qc.sttx40.com/Auth/api/v4/login",
    headers:
        { 'postman-token': '79ad1a33-c0e6-cc59-1918-b5bf0a1b0d11',
            'cache-control': 'no-cache',
            'content-type': 'application/json' },
    body: { username: 'qc.automation',
        password: '3Ppd0ae3uHir' ,
        location: 'CTS',
        sublocation: ''},
    json: true
};


describe("Login", function()  {
    it("Should login and get a token to be used later.", function() {
        this.timeout(4000);
        return new Promise(function(resolve) {
            return request.post(options3, function (error, response, body) {

                if (error) throw new Error(error);

                options.headers = {
                    'Authorization': "Bearer " + body.data,
                };

                return resolve();
            });
        });
    });
});

var options = {
    url: urlStart + "/ResultEntry/api/v4/ResultEntry/testoutcomes",
    body: {
        "TestId": "Kpc",
        "Subtests": [
            {
                "SubtestCode": "Anti-Kpc",
                "Phases": [
                    {
                        "PhaseCode": "CC",
                        "Result": "NT"
                    }
                ]
            }
        ]
    },
    json: true
};

describe("Test result entry array", function()  {

    it("Should get the result entry array", function() {
        this.timeout(4000);
        return new Promise( function(resolve) {
            return request.put(options, function (error, response, body) {

                if (error) throw new Error(error);

                body.errors["0"].code.should.be.eql(400159);

                return resolve();
            });
        });
    });
});