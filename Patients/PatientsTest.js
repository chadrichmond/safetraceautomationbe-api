var request = require("request");
var oracledb = require('oracledb');
let chai = require('chai');
let should = chai.should();
var urlStart = "https://qc.sttx40.com";
var options3 = {
    url: "https://qc.sttx40.com/Auth/api/v4/login",
    headers:
        { 'postman-token': '79ad1a33-c0e6-cc59-1918-b5bf0a1b0d11',
            'cache-control': 'no-cache',
            'content-type': 'application/json' },
    body: { username: 'qc.automation',
        password: '3Ppd0ae3uHir' ,
        location: 'CTS',
        sublocation: ''},
    json: true
};


describe("Login", function()  {
    it("Should login and get a token to be used later.", function() {
        return new Promise(function(resolve) {
            return request.post(options3, function (error, response, body) {

                if (error) throw new Error(error);

                options.headers = {
                    'Authorization': "Bearer " + body.data,
                };

                return resolve();
            });
        });
    });
});

var options = {
    url: urlStart + '/patients/api/v4/patient/1000',
};

describe("Test Patient ", function()  {

    it("Should get back a patient", function() {
        this.timeout(4000);
        return new Promise( function(resolve) {
            return request.get(options, function(error, response, body) {
                if (error) throw new Error(error);

                let resp = JSON.parse(body);
                let resp1 = resp.data["0"];

                resp1.aboCode.should.be.eql("O");
                resp1.patientId.should.be.eql("1000");
                resp1.mrn.should.be.eql("56454646");

                return resolve();
            });
        });
    });
});


describe("Test Patient txrx", function()  {

    it("Should get back a patient txrx list", function() {
        this.timeout(4000);
        return new Promise( function(resolve) {
            return request.get(urlStart + '/patients/api/v4/patient/1000/txrxs', options, function(error, response, body) {
                if (error) throw new Error(error);

                let resp = JSON.parse(body);
                let resp1 = resp.data["0"];

                resp1.patientTxReactionCode.should.be.eql(3367);
                resp.data["1"].patientTxReactionCode.should.be.eql(3365);

                return resolve();
            });
        });
    });
});




